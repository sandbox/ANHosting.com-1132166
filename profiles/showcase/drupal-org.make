core = 7.0

; Uses these modules:

projects[features] = 1.0-beta2
projects[ctools] = 1.0-alpha4
projects[views] = 3.0-beta3

; Showcase portfolio functionality module
projects[showcase_portfolio] = 1.0
; Showcase theme
projects[showcase] = 1.0